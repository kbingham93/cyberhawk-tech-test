import { defineConfig } from 'vitepress'
import { withMermaid } from "vitepress-plugin-mermaid";

// https://vitepress.dev/reference/site-config


export default withMermaid(defineConfig({
  title: "Cyberhawk Tech Test",
  description: "Documentation",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
    ],

    sidebar: [
      {
        text: 'How To',
        items: [
          { text: 'Site Navigation', link: '/' },
          { text: 'Project Setup', link: '/setup' },
        ]
      },
      {
        text: 'Documentation',
        items: [
          { text: 'Database', link: '/database' },
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ]
  },
  mermaidPlugin: {
    class: "mermaid my-class", // set additional css classes for parent container
  },
}));


