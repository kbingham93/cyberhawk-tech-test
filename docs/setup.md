# Project Setup

## Install composer dependencies

If you have PH 8.2 installed you can simply run:

``` bash
composer install
```

Alternatively, you can or use the following docker container that run PHP 8.2 to install the dependencies

``` bash
docker run --rm \
-u "$(id -u):$(id -g)" \
-v $(pwd):/var/www/html \
-w /var/www/html \
laravelsail/php81-composer:latest \
composer install --ignore-platform-reqs
```

Once the composer dependencies are installed, run the following commands to set up the .env file.

``` bash
cp .env.example .env
php artisan key:generate
php artisan cache:clear
php artisan config:clear
```

Now you are ready to start the projects docker container via sail:

``` bash
./vendor/bin/sail -d
```

Once start you will need to run the following commands to finalise the setup.

``` bash
./vendor/bin/sail sail artisan migrate
./vendor/bin/sail sail artisan db:seed
```

These will set up the database tables and populate them with test data.

You should now be able to access the website - http://localhost/login

You can log in with the test user details:

```
Email - test@example.com
Password TestPassword123
```

To view the documentation in the browser run the following command:

```
npm run docs:dev
```

You should now be able to open - http://localhost:5175/ in your browser

For the site instructions you can find them here - http://localhost:5175/


