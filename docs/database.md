---
outline: deep
---

# Database documentation

## Entity Relationship Diagram
### First iteration

The following database designs have been used to allow an turbine to have multiple inspections and have multiple components
that can be replaced or changed. That way each component will have a record of each inspection for future reference.

```mermaid
erDiagram
    user ||--|{ turbine : owns
    turbine ||--|{ component : has
    turbine }|--|{ inspection: has
    component }|--|{ inspection: has
    user ||--|{ inspection : writes
```

```mermaid
erDiagram
    user ||--|{ turbine : owns
    turbine ||--|{ component : has
    turbine ||--|{ turbine-inspection: has
    turbine-inspection }|--|| inspection: has
    component ||--|{ component-inspection: has
    component-inspection }|--|| inspection: has
    user ||--|{ inspection : writes
```

```mermaid
erDiagram
    user ||--|{ turbine : owns
    user {
        string uuid PK
        string name
        string email
        string password
    }
    turbine ||--|{ component : has
    turbine {
        string uid PK
        string user_id FK
        date date
        string status
    }
    turbine ||--|{ turbine-inspection: has
    turbine-inspection }|--|| inspection: has
    turbine-inspection {
        string uid PK
        string turbine_id FK
        string inspection_id FK
    }
    component ||--|{ component-inspection: has
    component {
        string uid PK
        string turbine_id FK
        string type
        string status
    }
    component-inspection }|--|| inspection: has
    component-inspection {
        string uid PK
        string inspection_id FK
        string component_id FK
        string status
    }
    user ||--|{ inspection : writes
   inspection {
        string uid PK
        string inspector_user_id FK
        string status
        string date
    }
```

### Second iteration
This iteration comes from the idea that each turbine would be within a turbine field, which would allow
us to map the location of the field on to a map.

As well, I came to realise that each field will have multiple users who would want to access the inspection reports.

```mermaid
erDiagram
    user }|--|{ turbine-fields : owns
    turbine-fields ||--|{ turbine : owns
    turbine ||--|{ component : has
    turbine }|--|{ inspection: has
    component }|--|{ inspection: has
    user ||--|{ inspection : writes
```

```mermaid
erDiagram
    user ||--|{ user-turbine-fields-access : access
    user-turbine-fields-access }|--|| turbine-fields : has
    turbine-fields ||--|{ turbine : has
    turbine ||--|{ component : has
    turbine ||--|{ turbine-inspection: has
    turbine-inspection }|--|| inspection: has
    component ||--|{ component-inspection: has
    component-inspection }|--|| inspection: has
    user ||--|{ inspection : writes
```

```mermaid
erDiagram
    user ||--|{ user-turbine-fields-access : access
    user {
        string uuid PK
        string name
        string email
        string password
        dateTime created_at
        dateTime updated_at
    }
    user-turbine-fields-access }|--|| turbine-fields : has
     user-turbine-fields-access {
        string uid PK
        string turbine_fields_id FK
        string user_id FK
    }
    turbine-fields ||--|{ turbine : has
    turbine-fields {
        string uid PK
        dateTime created_at
        dateTime updated_at
        string status
        string location
        string name
    }
    turbine ||--|{ component : has
    turbine {
        string uid PK
        string turbine_fields_id FK
        dateTime created_at
        dateTime updated_at
        string status
    }
    turbine ||--|{ turbine-inspection: has
    turbine-inspection }|--|| inspection: has
    turbine-inspection {
        string uid PK
        string turbine_id FK
        string inspection_id FK
    }
    component ||--|{ component-inspection: has
    component {
        string uid PK
        string turbine_id FK
        string type
        string status
        dateTime created_at
        dateTime updated_at
    }
    component-inspection }|--|| inspection: has
    component-inspection {
        string uid PK
        string inspection_id FK
        string component_id FK
        string status
    }
    user ||--|{ inspection : writes
    inspection {
        string uid PK
        string inspector_user_id FK
        string status
        dateTime created_at
        dateTime updated_at
    }
```
