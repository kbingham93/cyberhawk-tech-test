# How to navigate the site
Logging in and navigating the interface in localhost.

- 1 Navigate to http://localhost/login

- 2 Click the "Email" field.
  ![img.png](images/1.png)

- 3 Type "test@example.com"

- 4 Click the "Password" field.
  ![img.png](images/4.png)

- 5 Type "TestPassword

- 6 Click "LOGIN"
  ![img.png](images/6.png)

- 7 Click one of the turbine symbols on the map.
  ![img.png](images/7.png)

- 8 This will then give you a list of turbines, Click here, to view the turbine inspections.
  ![img.png](images/8.png)

- 9 You will then see the inspection reports with the component status
  ![img.png](images/9.png)

- 10 Click this icon, on another turbine to view another report.
  ![img.png](images/10.png)

- 11 Click another turbine symbol to view another wind farm turbine.
  ![img.png](images/11.png)


