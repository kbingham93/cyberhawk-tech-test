<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Component
 *
 * @property string $uuid
 * @property string $turbine_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @property int $status
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Inspection> $inspections
 * @property-read int|null $inspections_count
 * @property-read \App\Models\Turbine|null $turbines
 * @method static \Illuminate\Database\Eloquent\Builder|Component newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Component newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Component query()
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereTurbineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereUuid($value)
 */
	class Component extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Inspection
 *
 * @property string $uuid
 * @property string $turbine_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @property int $status
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Component> $components
 * @property-read int|null $components_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Turbine> $turbines
 * @property-read int|null $turbines_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection query()
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection whereTurbineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspection whereUuid($value)
 */
	class Inspection extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Turbine
 *
 * @property string $uuid
 * @property string $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Component> $components
 * @property-read int|null $components_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Inspection> $inspections
 * @property-read int|null $inspections_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine query()
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Turbine whereUuid($value)
 */
	class Turbine extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property string $uuid
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property mixed $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Inspection> $inspections
 * @property-read int|null $inspections_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Sanctum\PersonalAccessToken> $tokens
 * @property-read int|null $tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Turbine> $turbines
 * @property-read int|null $turbines_count
 * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUuid($value)
 */
	class User extends \Eloquent {}
}

