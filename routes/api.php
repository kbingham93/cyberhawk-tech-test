<?php

use App\Http\Controllers\TurbinesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/turbines/field/{uuid}', [TurbinesController::class, 'getFieldTurbines']);
Route::middleware('auth:sanctum')->get('/turbines/inspections/{uuid}', [TurbinesController::class, 'getTurbineInspections']);
