<?php

namespace App\Http\Controllers;

use App\Models\Turbine;
use App\Models\TurbineField;
use Illuminate\Http\Request;

class TurbinesController extends Controller
{
    public function getFieldTurbines(Request $request, String $uuid)
    {
        $field = TurbineField::findOrFail($uuid);

        $turbines = $field->turbines;

        $turbinesArray = $field->turbines->toArray();
        foreach ($turbines as $key => $turbine) {
            $turbinesArray[$key]['last_inspection'] =  date("d F Y", strtotime($turbine->latestInspections[0]->created_at));
        }

        return response()->json($turbinesArray);
    }

    public function getTurbineInspections(Request $request, String $uuid)
    {
        $turbine = Turbine::findOrFail($uuid);

        $inspections = $turbine->inspections;


        foreach ($inspections as $inspection) {
            $inspection->user;
            $inspection;
            $inspection->components;
        }

        return response()->json([
          $inspections->toArray()
        ]);
    }
}
