<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function getDashboard(Request $request)
    {
        $user = $request->user();

        $token = $user->createToken($user->uuid);

        $turbineFields = $user->turbineFields;

        return Inertia::render('Dashboard', [
          'turbineFields' => $turbineFields,
          'token' => $token->plainTextToken
        ]);
    }
}
