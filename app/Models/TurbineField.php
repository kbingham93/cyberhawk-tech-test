<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class TurbineField extends Model
{
    use HasFactory;

    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
      'status',
      'location',
      'name',
    ];

    /**
     * This function populates the uuid on new model creation
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    /**
     * Relationship definitions
     */

    /**
     * @return HasMany
     */
    public function turbines(): HasMany
    {
        return $this->HasMany(Turbine::class);
    }

    /**
     * @return BelongsToMany
     */
    public function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_turbine_fields_access');
    }

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
      'location' => 'array',
    ];
}
