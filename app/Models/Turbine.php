<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Turbine extends Model
{
    use HasFactory;

    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
      'turbine_field_id',
      'status',
    ];



    /**
     * This function populates the uuid on new model creation
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    /**
     * Relationship definitions
     */

    /**
     * @return BelongsTo
     */
    public function turbineField(): BelongsTo
    {
        return $this->belongsTo(TurbineField::class);
    }

    public function latestInspections(): BelongsToMany
    {
        return $this->belongsToMany(Inspection::class, 'turbine_inspections')->where('status', 'completed')->orderBy('created_at', 'DESC');
    }

    public function components(): HasMany
    {
        return $this->hasMany(Component::class);
    }

    /**
     * @return BelongsToMany
     */
    public function inspections(): BelongsToMany
    {
        return $this->belongsToMany(Inspection::class, 'turbine_inspections')->orderBy('created_at', 'DESC');
    }

}
