<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Inspection extends Model
{
    use HasFactory;

    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
      'inspector_user_uuid',
      'status',
    ];

    protected $casts = [
      'created_at' => "datetime:d F Y",
    ];

    /**
     * This function populates the uuid on new model creation
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    /**
     * Relationship definitions
     */

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'inspector_user_uuid');
    }

    /**
     * @return BelongsToMany
     */
    public function turbines(): BelongsToMany
    {
        return $this->belongsToMany(Turbine::class, 'turbine_inspections');
    }

    /**
     * @return BelongsToMany
     */
    public function components(): BelongsToMany
    {
        return $this->belongsToMany(Component::class, 'component_inspections')->withPivot('status');
    }
}
