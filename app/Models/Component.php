<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Component extends Model
{
    use HasFactory;

    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
      'turbine_id',
      'type',
      'status',
    ];

    /**
     * This function populates the uuid on new model creation
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    /**
     * Relationship definitions
     */

    /**
     * @return BelongsTo
     */
    public function turbines(): BelongsTo
    {
        return $this->belongsTo(Turbine::class);
    }

    /**
     * @return BelongsToMany
     */
    public function inspections(): BelongsToMany
    {
        return $this->belongsToMany(Inspection::class, 'component_inspections');
    }
}
