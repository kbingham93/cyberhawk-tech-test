module.exports = {
    root: true,
    parserOptions: {
        ecmaVersion: 2020, // Use the latest ecmascript standard
        sourceType: 'module', // Allows using import/export statements
        ecmaFeatures: {
            jsx: true, // Enable JSX since we're using React
        },
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
    env: {
        browser: true,
        amd: true,
        node: true,
    },
    extends: [
        'airbnb',
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:react-hooks/recommended',
        'plugin:prettier/recommended',
        'plugin:tailwindcss/recommended'
    ],
    plugins: ['prettier'],
    rules: {
        'react/jsx-first-prop-new-line': [2, 'multiline'],
        'react/jsx-max-props-per-line': [2, { maximum: 1, when: 'multiline' }],
        'react/jsx-indent-props': [2, 2],
        'react/jsx-closing-bracket-location': [2, 'tag-aligned'],
        'prettier/prettier': [
            'error',
            {},
            {
                usePrettierrc: true,
            },
        ],
        'react/react-in-jsx-scope': 'off',
        'react/prop-types': 'off',
        'no-console': 2,
        'import/extensions': 0,
        'import/no-unresolved': 0,
        'tailwindcss/no-custom-classname': ['warn', {
            cssFiles: [
                'resources/css/app.css',
                'resources/css/editor.css',
                'resources/css/font-delivery.css'
            ]
        }],
      'react/jsx-props-no-spreading': 0,
      'no-shadow': 0,
      'jsx-a11y/label-has-associated-control': [ 'error', {
        'required': {
          'some': [ 'nesting', 'id'  ]
        }
      }],
      'jsx-a11y/label-has-for': [ 'error', {
        'required': {
          'some': [ 'nesting', 'id'  ]
        }
      }],
      'react-hooks/exhaustive-deps': 0,
      'react/jsx-no-bind' : 0
    },
    settings: {
        'import/resolver': {
            node: {
                'extensions': ['.js', '.jsx']
            },
            alias: {
                map: [
                    ['@/', 'resources/js/.'],
                ],
            },
        },
    }
};
