import * as React from 'react';
import Map, { Marker } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

export default function MapBox(props) {
  const { turbineFields, callbackFunction } = props;

  return (
    <div className="grid grid-cols-3 gap-4">
      <div className="col-span-2">
        <Map
          mapboxAccessToken={import.meta.env.VITE_MAPBOX_API_KEY}
          initialViewState={{
            longitude: turbineFields[0].location.longitude,
            latitude: turbineFields[0].location.latitude,
            zoom: 3,
          }}
          style={{ height: 400 }}
          mapStyle="mapbox://styles/mapbox/streets-v9"
        >
          {turbineFields.map(field => {
            return (
              <Marker
                longitude={field.location.longitude}
                latitude={field.location.latitude}
                key={field.uuid}
                anchor="bottom"
                onClick={e => {
                  callbackFunction(field.uuid, e);
                }}
              >
                {field.status === 'active' && (
                  <img
                    src="./images/turbine-field-active.webp"
                    className="h-12 w-12"
                    alt="active turbine icon"
                    id={`field_${field.uuid}_marker`}
                  />
                )}
                {field.status === 'inactive' && (
                  <img
                    src="./images/turbine.webp"
                    width={50}
                    height={50}
                    className="h-12 w-12"
                    alt="inactive turbine icon"
                    id={`field_${field.uuid}_marker`}
                  />
                )}
                {field.status === 'inspection' && (
                  <img
                    src="./images/turbine-field-under-inspection.webp"
                    className="h-12 w-12"
                    alt="under inspection turbine icon"
                    id={`field_${field.uuid}_marker`}
                  />
                )}
                {field.status === 'maintenance' && (
                  <img
                    src="./images/turbine-field-under-maintanence.webp"
                    className="h-12 w-12"
                    alt="under maintanence turbine icon"
                    id={`field_${field.uuid}_marker`}
                  />
                )}
              </Marker>
            );
          })}
        </Map>
      </div>
      <div className="col-span-1 pl-6">
        <h1 className="font-extrabold underline">Key</h1>
        <h1 className="font-extrabold">Wind Farm Icons:</h1>
        <div className="grid grid-cols-2 gap-8 text-center">
          <span className="col-span-1">
            <img
              src="./images/turbine-field-active.webp"
              width={50}
              height={50}
              className="m-auto"
              alt="active turbine icon"
            />
            Active Wind Farm
          </span>
          <span className="col-span-1">
            <img
              src="./images/turbine.webp"
              width={50}
              height={50}
              className="m-auto"
              alt="inactive turbine icon"
            />
            Inactive Wind Farm
          </span>
          <span className="col-span-1">
            <img
              src="./images/turbine-field-under-inspection.webp"
              width={50}
              height={50}
              className="m-auto"
              alt="under inspection turbine icon"
            />
            Under Inspection
          </span>
          <span className="col-span-1">
            <img
              src="./images/turbine-field-under-maintanence.webp"
              width={50}
              height={50}
              className="m-auto"
              alt="under maintanence turbine icon"
            />
            Under Maintenance
          </span>
          <span className="col-span-2 text-left">
            Click on a turbine symbol to find out more information
          </span>
        </div>
      </div>
    </div>
  );
}
