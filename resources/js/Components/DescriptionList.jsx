export default function DescriptionList(props) {
  const { listItems } = props;

  return (
    <div>
      <div className="mt-6 border-t border-gray-100">
        <dl className="divide-y divide-gray-100">
          {listItems.map(item => (
            <div
              className="grid grid-cols-3 px-4 py-6 sm:gap-4 sm:px-0"
              key={item.uuid}
            >
              <span className="col-span-1">
                <p className="text-sm leading-6 text-gray-900">
                  <span className="font-extrabold">Report ID</span> -{' '}
                  {item.uuid}
                </p>
                <p className="text-sm leading-6 text-gray-900">
                  <span className="font-extrabold">Inspected By</span> -{' '}
                  {item.user.name}
                </p>
                <p className="text-sm leading-6 text-gray-900">
                  <span className="font-extrabold">Inspected Date</span> -{' '}
                  {item.created_at}
                </p>
              </span>
              <span className="col-span-1 mt-1 grid grid-cols-3 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">
                {item.components.length > 0 ? (
                  item.components.map(component => (
                    <p
                      key={component.uuid}
                      className="col-span-1 grid grid-cols-2"
                    >
                      {' '}
                      <span>{component.type}</span>{' '}
                      <span>Status: {component.pivot.status}</span>
                    </p>
                  ))
                ) : (
                  <h1 className="text-lg font-extrabold">
                    Awaiting for inspection report
                  </h1>
                )}
              </span>
            </div>
          ))}
        </dl>
      </div>
    </div>
  );
}
