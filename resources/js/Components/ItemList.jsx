export default function ItemList(props) {
  const { type, listItems, callback } = props;
  function images(item) {
    if (type === 'turbine') {
      if (item.status === 'active')
        return (
          <img
            src="./images/turbine-field-active.webp"
            className="h-12 w-12 flex-none rounded-full bg-gray-50"
            alt="active turbine icon"
          />
        );
      if (item.status === 'inactive')
        return (
          <img
            src="./images/turbine.webp"
            width={50}
            height={50}
            className="h-12 w-12 flex-none rounded-full bg-gray-50"
            alt="inactive turbine icon"
          />
        );
      if (item.status === 'inspection')
        return (
          <img
            src="./images/turbine-field-under-inspection.webp"
            width={50}
            height={50}
            className="h-12 w-12 flex-none rounded-full bg-gray-50"
            alt="under inspection turbine icon"
          />
        );

      if (item.status === 'maintenance')
        return (
          <img
            src="./images/turbine-field-under-maintanence.webp"
            width={50}
            height={50}
            className="h-12 w-12 flex-none rounded-full bg-gray-50"
            alt="under maintanence turbine icon"
          />
        );
    }
    return (
      <img
        src="./images/turbine.webp"
        width={50}
        height={50}
        className="h-12 w-12 flex-none rounded-full bg-gray-50"
        alt="inactive turbine icon"
      />
    );
  }

  function status(item) {
    if (item.status === 'active')
      return (
        <div className="flex-none rounded-full bg-emerald-500/20 p-1">
          <div className="h-1.5 w-1.5 rounded-full bg-emerald-500" />
        </div>
      );
    if (item.status === 'inactive')
      return (
        <div className="flex-none rounded-full bg-stone-900/20 p-1">
          <div className="h-1.5 w-1.5 rounded-full bg-stone-900" />
        </div>
      );
    if (item.status === 'inspection')
      return (
        <div className="flex-none rounded-full bg-amber-500/20 p-1">
          <div className="h-1.5 w-1.5 rounded-full bg-amber-500" />
        </div>
      );

    if (item.status === 'maintenance')
      return (
        <div className="flex-none rounded-full bg-red-500/20 p-1">
          <div className="h-1.5 w-1.5 rounded-full bg-red-500" />
        </div>
      );

    return '';
  }

  if (type === 'turbine') {
    return (
      <ul className="divide-y divide-gray-100">
        {listItems.map(item => (
          <li key={item.uuid} className="flex justify-between gap-x-6 py-5">
            <div className="flex min-w-0 gap-x-4">
              {images(item)}

              <div className="grid grid-cols-2 items-stretch  gap-x-6 pt-3 align-middle">
                <p className="col-span-1 text-sm font-semibold leading-6 text-gray-900">
                  ID - {item.uuid}
                </p>
                <p className="col-span-1 text-sm font-semibold leading-6 text-gray-900">
                  Last Inspected - {item.last_inspection}
                </p>
              </div>
            </div>
            <div className="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
              <div className="mt-1 flex items-center gap-x-1.5">
                <p className="text-xs leading-5 text-gray-500">
                  {' '}
                  {item.status === 'maintenance' || item.status === 'inspection'
                    ? 'Under'
                    : ''}{' '}
                  {item.status.charAt(0).toUpperCase() + item.status.slice(1)}
                </p>
                {status(item)}
                <button
                  type="button"
                  className="pl-5 text-xs text-gray-500"
                  id={`inspection-details_${item.uuid}`}
                  onClick={e => {
                    callback(item.uuid, e);
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="h-6 w-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M12 7.5h1.5m-1.5 3h1.5m-7.5 3h7.5m-7.5 3h7.5m3-9h3.375c.621 0 1.125.504 1.125 1.125V18a2.25 2.25 0 01-2.25 2.25M16.5 7.5V18a2.25 2.25 0 002.25 2.25M16.5 7.5V4.875c0-.621-.504-1.125-1.125-1.125H4.125C3.504 3.75 3 4.254 3 4.875V18a2.25 2.25 0 002.25 2.25h13.5M6 7.5h3v3H6v-3z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </li>
        ))}
      </ul>
    );
  }
}
