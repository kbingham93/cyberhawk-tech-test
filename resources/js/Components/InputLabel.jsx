export default function InputLabel({
  value,
  className = '',
  children,
  ...props
}) {
  return (
    /* eslint-disable jsx-a11y/label-has-for */
    <label
      {...props}
      className={`block text-sm font-medium text-gray-700 dark:text-gray-300 ${className}`}
    >
      {value || children}
    </label>
  );
}
