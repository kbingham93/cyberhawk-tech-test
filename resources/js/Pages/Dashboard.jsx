import { Head } from '@inertiajs/react';
import { useState } from 'react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import MapBox from '@/Components/MapBox';
import ItemList from '@/Components/ItemList.jsx';
import DescriptionList from '@/Components/DescriptionList.jsx';

export default function Dashboard({ auth, turbineFields }) {
  const token = '';
  const [turbines, setTurbines] = useState([]);
  const [selectedField, setSelectedField] = useState([]);
  const [inspections, setInspections] = useState([]);
  const [turbineUUID, setTurbineUUID] = useState([]);
  function showTurbineFieldInformation(uuid) {
    setSelectedField(turbineFields.find(field => field.uuid === uuid));
    fetch(`/api/turbines/field/${uuid}`, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: token,
      },
    })
      .then(res => res.json())
      .then(result => {
        setTurbines(result);
      });
  }

  function showTurbineInspections(uuid) {
    setTurbineUUID(uuid);
    fetch(`/api/turbines/inspections/${uuid}`, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: token,
      },
    })
      .then(res => res.json())
      .then(result => {
        setInspections(result[0]);
      });
  }

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={
        <h2 className="text-xl font-semibold leading-tight text-gray-800 dark:text-gray-200">
          Wind Farms
        </h2>
      }
    >
      <Head title="Wind Farms" />

      <div className="py-12">
        <div className="mx-auto max-w-7xl sm:px-6 lg:px-8">
          <div className="overflow-hidden bg-white shadow-sm dark:bg-gray-800 sm:rounded-lg">
            <div className="p-6 text-gray-900 dark:text-gray-100">
              <MapBox
                turbineFields={turbineFields}
                callbackFunction={showTurbineFieldInformation}
              />
            </div>
          </div>
          <div id="turbine-field" />
          {turbines.length > 0 && (
            <div className="mt-5 overflow-hidden bg-white shadow-sm dark:bg-gray-800 sm:rounded-lg">
              <div className="p-6 text-gray-900 dark:text-gray-100">
                <h1 className="text-xl">
                  {selectedField.name} Turbines:
                  <ItemList
                    listItems={turbines}
                    type="turbine"
                    callback={showTurbineInspections}
                  />
                </h1>
              </div>
            </div>
          )}
          {inspections.length > 0 && (
            <div
              className="mt-5 overflow-hidden bg-white shadow-sm dark:bg-gray-800 sm:rounded-lg"
              id="inspectionList"
            >
              <div className="p-6 text-gray-900 dark:text-gray-100">
                <h1 className="text-xl">
                  {selectedField.name} Turbine Inspections for: {turbineUUID}
                </h1>
                <DescriptionList listItems={inspections} />
              </div>
            </div>
          )}
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
