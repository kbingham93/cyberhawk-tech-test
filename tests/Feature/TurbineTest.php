<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class TurbineTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // seed the database
        $this->artisan('db:seed');
    }

    public function test_user_can_fetch_turbine_information(): void
    {
        $user = User::where('email', 'test@example.com')->first();
        $fields = $user->turbineFields;

        $response = $this
          ->actingAs($user)
          ->get('/api/turbines/field/' .  $fields[0]->uuid);

        $response
          ->assertStatus(Response::HTTP_OK)
          ->assertJsonStructure([
            '*' => [
              'uuid',
              'created_at',
              'status',
              'updated_at',

            ]
          ]);
    }

    public function test_none_user_can_not_access_turbines(): void
    {
        $user = User::where('email', 'test@example.com')->first();
        $fields = $user->turbineFields;

        $response = $this
          ->get('/api/turbines/field/' .  $fields[0]->uuid);

        $response
          ->assertStatus(Response::HTTP_FOUND);
    }

    public function test_user_can_fetch_inspection_reports(): void
    {
        $user = User::where('email', 'test@example.com')->first();
        $fields = $user->turbineFields;
        $turbines = $fields[0]->turbines;

        $response = $this
            ->actingAs($user)
            ->get('/api/turbines/inspections/' .  $turbines[0]->uuid);

        $response
          ->assertStatus(Response::HTTP_OK);
    }



    public function test_none_user_can_not_access_turbine_inspections(): void
    {
        $user = User::where('email', 'test@example.com')->first();
        $fields = $user->turbineFields;

        $response = $this
          ->get('/api/turbines/field/' .  $fields[0]->uuid);

        $response
          ->assertStatus(Response::HTTP_FOUND);
    }
}
