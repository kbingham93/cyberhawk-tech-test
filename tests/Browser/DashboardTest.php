<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\DashboardPage;
use Tests\DuskTestCase;

class DashboardTest extends DuskTestCase
{
    public function test_visiting_dashboard_when_not_authenticated(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/dashboard')->assertSee('Email');
            $browser->assertPathIs('/login');
        });
    }

    public function test_visiting_dashboard_when_authenticated(): void
    {
        $this->browse(function (Browser $browser) {
            $dashboard = new DashboardPage();
            $browser->loginAs(User::where('email', 'test@example.com')->first())
                    ->visit($dashboard)->assertSee('Wind Farm');
            $browser->screenshot('dashboard');
        });
    }

    public function test_clicking_on_windfarm_within_map(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::where('email', 'test@example.com')->first();
            $fields = $user->turbineFields->toArray();

            $dashboard = new DashboardPage();
            $browser->loginAs($user)
                    ->visit($dashboard)
                    ->assertSee('Wind Farm')
                   ->click('#field_' . $fields[0]['uuid'] . '_marker')
              ->assertSee($fields[0]['name']);
            $browser->screenshot('dashboard-turbine-clicked');

        });
    }

    public function test_clicking_on_turbine_inspection_details(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::where('email', 'test@example.com')->first();
            $fields = $user->turbineFields->toArray();
            $turbines = $user->turbineFields[0]->turbines->toArray();

            $dashboard = new DashboardPage();
            $browser->loginAs($user)
                    ->visit($dashboard)
                    ->assertSee('Wind Farm')
                    ->click('#field_' . $fields[0]['uuid'] . '_marker')
                    ->assertSee($fields[0]['name'])
            ->press('#inspection-details_' . $turbines[0]['uuid']);

            $browser->screenshot('dashboard-turbine-inspection-clicked');

        });
    }
}
