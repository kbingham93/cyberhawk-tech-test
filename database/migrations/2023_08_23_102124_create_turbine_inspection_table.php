<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('turbine_inspections', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->string('turbine_uuid');
            $table->string('inspection_uuid');
            $table->timestamps();
            $table->foreign('turbine_uuid')->references('uuid')->on('turbines');
            $table->foreign('inspection_uuid')->references('uuid')->on('inspections');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('turbine_inspections');
    }
};
