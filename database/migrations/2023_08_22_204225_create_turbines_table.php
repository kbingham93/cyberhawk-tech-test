<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('turbines', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->string('turbine_field_uuid');
            $table->timestamps();
            $table->string('status');
            $table->foreign('turbine_field_uuid')->references('uuid')->on('turbine_fields');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('turbines');
    }
};
