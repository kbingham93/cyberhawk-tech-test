<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('component_inspections', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->string('component_uuid');
            $table->string('inspection_uuid');
            $table->timestamps();
            $table->integer('status');
            $table->foreign('component_uuid')->references('uuid')->on('components');
            $table->foreign('inspection_uuid')->references('uuid')->on('inspections');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('component_inspections');
    }
};
