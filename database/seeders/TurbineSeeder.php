<?php

namespace Database\Seeders;

use App\Models\Turbine;
use App\Models\TurbineField;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TurbineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $turbineFarms = TurbineField::get();

        foreach($turbineFarms as $farm) {
            $turbines = [
              [
                'uuid'               => Str::uuid(),
                'status'             => config('constants.turbine_relevant_status.active'),
                'turbine_field_uuid' => $farm->uuid,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'         => Carbon::now()->format('Y-m-d H:i:s')
              ],
              [
                'uuid'               => Str::uuid(),
                'status'             => config('constants.turbine_relevant_status.maintenance'),
                'turbine_field_uuid' => $farm->uuid,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'         => Carbon::now()->format('Y-m-d H:i:s')
              ],
              [
                'uuid'               => Str::uuid(),
                'status'             => config('constants.turbine_relevant_status.inactive'),
                'turbine_field_uuid' => $farm->uuid,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'         => Carbon::now()->format('Y-m-d H:i:s')
              ],
              [
                'uuid'               => Str::uuid(),
                'status'             => config('constants.turbine_relevant_status.inspection'),
                'turbine_field_uuid' => $farm->uuid,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'         => Carbon::now()->format('Y-m-d H:i:s')
              ],
              [
                'uuid'               => Str::uuid(),
                'status'             => config('constants.turbine_relevant_status.active'),
                'turbine_field_uuid' => $farm->uuid,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'         => Carbon::now()->format('Y-m-d H:i:s')
              ],
            ];

            Turbine::insert($turbines);
        }
    }
}
