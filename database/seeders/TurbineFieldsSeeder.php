<?php

namespace Database\Seeders;

use App\Models\TurbineField;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TurbineFieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $testUser = User::where('email', 'test@example.com')->first();
        $user = User::where('email', 'kylebingham93@gmail.com')->first();

        $turbineFields = [
          [
            'name' => 'Penny Hill Wind Farm',
            'location' => ['latitude' => 53.386447, 'longitude' => -1.283560],
            'status' => config('constants.turbine_field_status.active'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ],
          [
            'name' => 'Barrow',
            'location' => ['latitude' => 53.5900, 'longitude' => 3.1700],
            'status' => config('constants.turbine_field_status.inactive'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ],
          [
            'name' => 'Beatrice',
            'location' => ['latitude' => 58.5900, 'longitude' => 3.0535],
            'status' => config('constants.turbine_field_status.underMaintenance'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ],
          [
            'name' => 'East Anglia One',
            'location' => ['latitude' => 52.1404, 'longitude' => 2.2918],
            'status' => config('constants.turbine_field_status.active'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ],
          [
            'name' => 'Greater Gabbard',
            'location' => ['latitude' => 51.560, 'longitude' => 1.53],
            'status' => config('constants.turbine_field_status.inspection'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ],
          [
            'name' => 'Triton Knoll',
            'location' => ['latitude' => 53.3000, 'longitude' => 0.480],
            'status' => config('constants.turbine_field_status.underMaintenance'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ]
        ];

        foreach($turbineFields as $turbineField) {
            $field = TurbineField::create($turbineField);

            DB::table('user_turbine_fields_access')->insert([
              'uuid' => Str::uuid(),
              'turbine_field_uuid' => $field->uuid,
              'user_uuid' => $testUser->uuid,
              'status' => config('constants.turbine_field_status.active'),
              'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
              'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            DB::table('user_turbine_fields_access')->insert([
              'uuid' => Str::uuid(),
              'turbine_field_uuid' => $field->uuid,
              'user_uuid' => $user->uuid,
              'status' => config('constants.turbine_field_status.active'),
              'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
              'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }





    }
}
