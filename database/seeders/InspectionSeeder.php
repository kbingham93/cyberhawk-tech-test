<?php

namespace Database\Seeders;

use App\Models\Inspection;
use App\Models\Turbine;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InspectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws \Exception
     */
    public function run(): void
    {
        $turbines = Turbine::get();
        $user = User::where('email', 'kylebingham93@gmail.com')->first();

        foreach($turbines as $turbine) {


            $inspections = [
              [
                'uuid'               => Str::uuid(),
                'inspector_user_uuid' => $user->uuid,
                'status' => $turbine->status !== 'inspection' ? 'completed' : 'in-progress',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
              ],
              [
                'uuid'               => Str::uuid(),
                'inspector_user_uuid' => $user->uuid,
                'status' => 'completed',
                'created_at' => Carbon::now()->subMonths(1)->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->subMonths(1)->format('Y-m-d H:i:s')
              ],
              [
                'uuid'               => Str::uuid(),
                'inspector_user_uuid' => $user->uuid,
                'status' => 'completed',
                'created_at' => Carbon::now()->subMonths(2)->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->subMonths(2)->format('Y-m-d H:i:s')
              ],
              [
                'uuid'               => Str::uuid(),
                'inspector_user_uuid' => $user->uuid,
                'status' => 'completed',
                'created_at' => Carbon::now()->subMonths(3)->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->subMonths(3)->format('Y-m-d H:i:s')
              ]
            ];

            foreach($inspections as $inspection) {
                $inspectionInserted = Inspection::create($inspection);

                DB::table('turbine_inspections')->insert([
                  'uuid' => Str::uuid(),
                  'turbine_uuid' => $turbine->uuid,
                  'inspection_uuid' => $inspectionInserted->uuid,
                  'created_at' => $inspectionInserted->created_at,
                  'updated_at' => $inspectionInserted->updated_at,
                ]);

                if ($inspection['status'] === 'completed') {
                    $components = $turbine->components;

                    foreach ($components as $component) {
                        DB::table('component_inspections')->insert([
                          'uuid'            => Str::uuid(),
                          'component_uuid'  => $component->uuid,
                          'inspection_uuid' => $inspectionInserted->uuid,
                          'created_at'      => $inspectionInserted->created_at,
                          'updated_at'      => $inspectionInserted->updated_at,
                          'status'          => random_int(1, 5)
                        ]);
                    }
                }
            }
        }
    }
}
