<?php

namespace Database\Seeders;

use App\Models\Component;
use App\Models\Turbine;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ComponentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $turbines = Turbine::all();

        foreach ($turbines as $turbine) {
            $components = [
              ['uuid'       => Str::uuid(),
               'status'     => config('constants.turbine_relevant_status.active'),
               'turbine_uuid' => $turbine->uuid,
               'type'       => 'Blade',
               'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
               'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
              ],
              ['uuid'       => Str::uuid(),
               'status'     => config('constants.turbine_relevant_status.active'),
               'turbine_uuid' => $turbine->uuid,
               'type'       => 'Blade',
               'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
               'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
              ],
              ['uuid'       => Str::uuid(),
               'status'     => config('constants.turbine_relevant_status.active'),
               'turbine_uuid' => $turbine->uuid,
               'type'       => 'Blade',
               'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
               'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
              ],
              ['uuid'       => Str::uuid(),
               'status'     => config('constants.turbine_relevant_status.active'),
               'turbine_uuid' => $turbine->uuid,
               'type'       => 'Rotor',
               'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
               'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
              ],
              ['uuid'       => Str::uuid(),
               'status'     => config('constants.turbine_relevant_status.active'),
               'turbine_uuid' => $turbine->uuid,
               'type'       => 'Hub',
               'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
               'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
              ],
              ['uuid'       => Str::uuid(),
               'status'     => config('constants.turbine_relevant_status.active'),
               'turbine_uuid' => $turbine->uuid,
               'type'       => 'Generator',
               'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
               'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
              ]
            ];

            Component::insert($components);
        }
    }
}
