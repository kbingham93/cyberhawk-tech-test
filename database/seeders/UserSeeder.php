<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
          'name' => 'Test User',
          'email' => 'test@example.com',
          'password' => Hash::make('TestPassword123'),
          'status' => config('constants.user_status.active')
        ]);

        User::factory()->create([
          'name' => 'Kyle Bingham',
          'email' => 'kylebingham93@gmail.com',
          'password' => Hash::make('TestPassword123'),
          'status' => config('constants.user_status.active')
        ]);
    }
}
