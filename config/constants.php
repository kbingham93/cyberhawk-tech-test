<?php

return [
  'user_status' => [
    'active' => 'active',
    'inactive' => 'inactive',
  ],
  'turbine_field_status' => [
    'active' => 'active',
    'inactive' => 'inactive',
    'inspection' => 'inspection',
    'underMaintenance' => 'maintenance',
  ],
  'turbine_relevant_status' => [
    'active' => 'active',
    'inactive' => 'inactive',
    'inspection' => 'inspection',
    'maintenance' => 'maintenance',
  ]
];
